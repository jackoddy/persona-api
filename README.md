# Persona API

This is a fake RESTful API that delivers made up data on a few endpoints. The data sits within a zip file and is decompressed automatically and seeded into a MongoDB instance running in a container, managed by `docker-compose`. This seeding happens inside a dedicated container which then exits - maintaining the notion of emphemeral containers. The Persona-API itself pings the seed container during start up and waits for an error response before starting up, to ensure that the database is ready and waiting.

## Running the Server

This project requires you to have:
 - Docker

```bash
  # inside /personal-api/
  $ docker-compose up
```

This command while build the containers and spin them up in the correct order. Once the MongoDB has been correctly started up and seeded then the Persona-API will start and be ready on `localhost:5000`.

## Querying the API

| method | url  | params  | response  | description  |
|---|---|---|---|---|
| `GET`  | /search/:username  | n/a  | json user object  | search the database for a user with specified username  |
| `GET`  | /people | page (int)  | json user array  | search the database for users - returning 25 users at a time  |
| `DELETE`  | /people/:username  | n/a  | n/a  | delete a user with specified username  |


#### side note

As Python is a new language to me the testing of this unfortunately has eluded me. An attempt as been made in the `/tests` folder but after much head scratching I decided I had spent enough time on this. During the head scratching I did also attempt to set up an end-to-end test container in node to hit all the endpoints to black-box test the API although unfortunately docker-compose was not cooperative in this case.

