from flask import Flask, request
import asyncio

from model import people
import utils.responses as responses



app = Flask(__name__)
loop = asyncio.get_event_loop()



@app.route('/search/<username>', methods=['GET'])
def get_user(username):
    user = loop.run_until_complete(people.get_one(username))

    if not user:
        return responses.resource_not_found()

    return responses.resource_found(user)

@app.route('/people', methods=['GET'])
def get_many_users():
    page_number = request.args.get('page')
    users = loop.run_until_complete(people.get_page(page_number))

    if not len(users) > 0:
        return responses.resource_not_found()

    return responses.resource_found(users)

@app.route('/people/<username>', methods=['DELETE'])
def delete_user(username):
    delete_result = loop.run_until_complete(people.delete_one(username))

    if not delete_result.deleted_count:
        return responses.resource_not_found()

    return responses.resource_deleted()
