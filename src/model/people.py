from utils.database import people
from utils.pagination import number_to_skip, PAGE_SIZE


async def get_one(username):
    result = await people.find_one({'username': username},
                                   {'_id': False},)
    return result

async def delete_one(username):
    result = await people.delete_one({'username': username})
    return result

async def get_page(page_number):
    result = await people.find({}, {'_id': False},
                               batch_size=PAGE_SIZE,
                               skip=number_to_skip(page_number)
    ).to_list(PAGE_SIZE)

    return result
