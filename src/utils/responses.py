from flask import Response
import json

def resource_found(item):
    return Response(
        response= json.dumps(item),
        status=200,
        content_type='application/json',
    )

def resource_not_found():
    return Response(
        response=json.dumps({'message': 'Could not find the requested resource'}),
        status=404,
        content_type='application/json',
    )

def resource_deleted():
    return Response(
        status=204,
    )
