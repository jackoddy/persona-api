import asyncio
import motor.motor_asyncio

client = motor.motor_asyncio.AsyncIOMotorClient('mongo', 27017)
db = client.personas
people = db.people
