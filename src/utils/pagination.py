PAGE_SIZE = 25

def number_to_skip(page_number):
    if not page_number:
        return 0

    n = int(page_number) - 1

    return n * PAGE_SIZE
