from python:3.6.5

workdir /usr/src/app

copy ./requirements.txt ./

run pip install -r ./requirements.txt
run pip install --no-cache-dir flask

copy ./src ./src

env FLASK_APP src/app.py

cmd flask run --host=0.0.0.0 --port=5000
