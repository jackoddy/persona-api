import unittest
import asyncio

from utils import database
from model import people

loop = asyncio.get_event_loop()

class TestPeopleModel(unittest.TestCase):

    def test_get_one_calls_find_one(self):
        database.people.find_one = unittest.mock.MagicMock()
        loop.run_until_complete(people.get_one('testuser')

        database.people.find_one.assert_called_with(
            {'username': 'testuser'},
            {'_id': False}
        )

    def test_delete_one_calls_delete_one(self):
        database.people.delete_one = unittest.mock.MagicMock()
        loop.run_until_complete(people.delete_one('testuser')

        database.people.delete_one.assert_called_with(
            {'username': 'testuser'},
        )

    def test_get_page_calls_find(self):
        database.people.find = unittest.mock.MagicMock()
        loop.run_until_complete(people.find'testuser')

        database.people.find.assert_called_with(
            {'username': 'testuser'},
        )


if __name__ == '__main__':
    unittest.main()
